<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::view('/', 'pages.home')->name('home');

Route::group(['prefix' => 'primary', 'namespace' => 'Primary'], function() {
    Route::resource('roles', 'RoleController');
});
