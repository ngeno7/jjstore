<?php

namespace App\Http\Controllers\Primary;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    public function index(Request $request) 
    {
        return view('pages.roles.index', [
            'data' => Role::all()
        ]);
    }
    public function create() 
    {
        $this->authorize('roles.create');

        return view('pages.roles.form',[
            'data' => new Role
        ]);
    }
    public function store(RoleRequest $request) 
    {
        $this->authorize('roles.create');
        $data = $request->all();
        $data['permissions'] = json_encode($request->input('permissions')) ?? json_encode([]);
        Role::create($data);

        session('type', 'info');
        session('message', 'Role added sucessfully');

        return redirect()->route('roles.index');
    }
    public function edit($code) 
    {
        $this->authorize('roles.update');
        $role = Role::where('code', $code)->firstOrFail();

        return view('pages.form', [
            'data' => $role
        ]);
    }
    public function update(RoleRequest $request, $code) 
    {
        $this->authorize('roles.update');
        $role = Role::where('code', $code)->firstOrFail();
        $data = $request->all();
        $data['permissions'] = json_encode($request->input('permissions')) ?? json_encode([]);
        $role->update($data);

        session('type', 'info');
        session('message', 'Role updated sucessfully');

        return redirect()->route('roles.index');
    }
    public function destroy($code) 
    {
        $this->authorize('roles.delete');

        $role = Role::where('code', $code)->firstOrFail();
        $role->delete();

        session('type', 'info');
        session('message', 'Role deleted sucessfully');

        return redirect()->route('roles.index');
    }
}
