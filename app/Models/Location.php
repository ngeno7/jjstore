<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $fillable = [ 'code', 'name', 'description', 'deleted_at'];

    protected $dates = ['deleted_at'];
}
