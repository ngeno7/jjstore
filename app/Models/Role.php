<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $fillable = ['code', 'name', 'slug', 'permissions', 'description', 'deleted_at'];

    protected $dates = ['deleted_at'];

    const PERMISSIONS = [
        'roles' => [
            'create', 'update', 'view', 'delete',
        ],
        'aircrafts' => [
            'create', 'update', 'view', 'delete',
        ],
        'tools' => [
            'create', 'update', 'view', 'delete',
        ],
        'inventory' => [
            'create', 'update', 'view', 'delete',
        ],
        'inventory_items' => [
            'create', 'update', 'view', 'delete',
        ],
        'requests' => [
            'create', 'update', 'view', 'delete',
        ],
    ];
}
