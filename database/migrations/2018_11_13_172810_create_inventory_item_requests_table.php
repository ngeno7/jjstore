<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Store\Enum\EnumConsts;

class CreateInventoryItemRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_item_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->string('reference', 50);
            $table->integer('user_id')->unsigned();
            $table->integer('store_manager_id')->unsigned()->nullable();
            $table->integer('aircraft_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->boolean('status')->default(true);
            $table->integer('priority')->default(EnumConsts::PRIORITY_one);
            $table->string('rid', 20)->nullable();
            $table->string('comment', 120)->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->string('siv_path', 120)->nullable();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('aircraft_id')->references('id')->on('aircrafts');
            $table->foreign('store_manager_id')->references('id')->on('users');
            $table->foreign('location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_item_requests');
    }
}
