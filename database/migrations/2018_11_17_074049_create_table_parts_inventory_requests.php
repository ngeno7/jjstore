<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Store\Enum\EnumConsts;

class CreateTablePartsInventoryRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts_inventory_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->integer('inventory_item_request_id')->unsigned();
            $table->integer('inventory_id')->unsigned();
            $table->integer('inventory_item_on_id')->unsigned()->nullable();
            $table->integer('inventory_item_off_id')->unsigned()->nullable();
            $table->string('r_quantity', 10)->default('0');
            $table->string('approved_quantity', 10)->default('0');
            $table->string('remaining_quantity', 15)->default('0.00');
            $table->string('position', 100)->nullable();
            $table->string('defect', 150)->nullable();
            $table->string('request_reason')->nullable();
            $table->integer('status')->default(EnumConsts::STATUS_pending);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
            $table->foreign('inventory_item_request_id')
                ->references('id')->on('inventory_item_requests');
            $table->foreign('inventory_item_on_id')
                ->references('id')->on('inventory_items');
            $table->foreign('inventory_item_off_id')
                ->references('id')->on('inventory_items');
            $table->foreign('inventory_id')
                ->references('id')->on('inventories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts_inventory_requests');
    }
}
