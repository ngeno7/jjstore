<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTopUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_top_ups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->integer('inventory_item_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->string('unit_price', 15)->default('0');
            $table->string('quantity', 15)->default('0');
            $table->boolean('loaned')->default(false);
            $table->string('receipt_number', 30)->nullable();
            $table->string('comment', 150)->nullable();
            $table->boolean('servicable')->default(false);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('inventory_item_id')
                ->references('id')->on('inventory_items');
            $table->foreign('location_id')
                ->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_top_ups');
    }
}
