<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->string('name', 120);
            $table->string('serial_number', 120)->nullable();
            $table->string('quantity', 10)->default('0');
            $table->string('description', 150)->nullable();
            $table->string('part_number', 120)->nullable();
            $table->string('specifications')->nullable();
            $table->string('calibration_date', 50)->nullable();
            $table->string('calibration_due_date', 50)->nullable();
            $table->string('model', 100)->nullable();
            $table->integer('supplier_id')->unsigned()->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tools');
    }
}
