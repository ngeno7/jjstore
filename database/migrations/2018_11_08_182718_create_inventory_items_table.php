<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->integer('inventory_id')->unsigned();
            $table->string('inventory_code', 20)->nullable();
            $table->string('serial_number', 20)->nullable();
            $table->string('part_number', 20)->nullable();
            $table->string('quantity')->default('0');
            $table->boolean('servicable')->default(false);
            $table->string('description', 150)->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->boolean('rotable')->default(false);
            $table->string('uom', 20)->nullable();
            $table->string('batch_number', 75)->nullable();
            $table->string('alternate_part_number', 75)->nullable();
            $table->string('spare_condition')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('inventory_id')
                ->references('id')->on('inventories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_items');
    }
}
