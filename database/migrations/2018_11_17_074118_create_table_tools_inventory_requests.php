<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Store\Enum\EnumConsts;

class CreateTableToolsInventoryRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tools_inventory_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->integer('inventory_item_request_id')->unsigned();
            $table->integer('tool_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->string('r_quantity', 10)->default('0');
            $table->string('approved_quantity', 10)->default('0');
            $table->boolean('returned')->default(false);
            $table->boolean('loan_recorded')->default(false);
            $table->string('comment', 150)->nullable();
            $table->string('receipt_number', 50)->nullable();
            $table->dateTime('time_in')->nullable();
            $table->dateTime('time_out')->nullable();
            $table->string('rate', 10)->default('0.00');
            $table->integer('status')->default(EnumConsts::STATUS_pending);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
            
            $table->foreign('location_id')
                ->references('id')->on('locations');
            $table->foreign('inventory_item_request_id')
                ->references('id')->on('inventory_item_requests');
            $table->foreign('tool_id')->references('id')
                ->on('tools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tools_inventory_requests');
    }
}
