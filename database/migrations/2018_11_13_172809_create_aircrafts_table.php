<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAircraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircrafts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->string('name', 50);
            $table->string('description', 100)->nullable();
            $table->boolean('active')->default(true);
            $table->string('tail_reg', 100)->nullable();
            $table->string('model_type', 100)->nullable();
            $table->string('serial_number', 100)->nullable();
            $table->string('engine_model', 100)->nullable();
            $table->string('owner')->nullable();
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircrafts');
    }
}
