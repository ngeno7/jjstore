<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('code', 50)->unique();
            $table->string('username', 30)->unique();
            $table->string('first_name', 80);
            $table->string('last_name', 80);
            $table->string('id_number', 20)->nullable();
            $table->string('employee_code', 20)->nullable();
            $table->string('password');
            $table->integer('location_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->dateTime('delete_at')->nullable();
            $table->timestamps();

            $table->foreign('location_id')
                ->references('id')->on('locations');
            $table->foreign('role_id')
                ->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
