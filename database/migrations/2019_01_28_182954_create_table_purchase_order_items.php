<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePurchaseOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_order_id')->unsigned();
            $table->integer('inventory_id');
            $table->string('part_number', 50)->nullable();
            $table->string('due_date', 50)->nullable();
            $table->string('uom', 50)->nullable();
            $table->string('quantity', 50)->nullable();
            $table->string('condition', 120)->nullable();
            $table->string('price', 50)->nullable();
            $table->string('line_value', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_items');
    }
}
