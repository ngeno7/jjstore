<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'code' => uniqid(),
            'username' => 'admin@jambojet.com',
            'first_name' => 'enock',
            'last_name' => 'langat',
            'id_number' => '565645461',
            'location_id' => 1,
            'role_id' => 1,
            'password' => Hash::make('jamboenock254')
        ]);
    }
}
