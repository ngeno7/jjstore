<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        Role::insert([
            [
                'code' => uniqid(),
                'name' => 'Admin',
                'slug' => 'admin',
                'permissions' => json_encode($this->getPermissions()),
                'description' => 'able to access admin portal',
                'created_at' => Carbon::now(),
            ],
        ]);

    }
    /**
     * Helper private method
     *
     * @return array
     */
    private function getPermissions() 
    {
        $permissions = [];
        foreach (Role::PERMISSIONS as $key => $permissionGroup) {
            foreach ($permissionGroup as $permission) {
                $permissions[] = $key.'.'.$permission;
            }
        }
        return $permissions;
    }
}
