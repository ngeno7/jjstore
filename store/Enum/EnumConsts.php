<?php

namespace Store\Enum;

abstract class EnumConsts {

    const STATUS_pending = 1;
    const STATUS_approved = 2;
    const STATUS_unavailable = 3;
    const STATUS_denied = 4;
    const STATUS_loaned = 5;

    const PRIORITY_one = 5;
    const PRIORITY_two = 6;
    const PRIORITY_three = 7;
}
